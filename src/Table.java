/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
public class Table {
    char data[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };;
	Player one;
	Player two;
	private Player currentPlayer;
	private Player win = null;
	int lastrow;
	int lastcol;
	int turn = 0;

	public Table(Player one, Player two) {
		this.one = one;
		this.two = two;
		this.currentPlayer = this.one;

	}

	public char[][] getData() {
		return data;
	}

	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	public void switchPlayer() {
		if (this.currentPlayer == one) {
			this.currentPlayer = two;
		} else {
			this.currentPlayer = one;
		}
	}

	public boolean setRowCol(int row, int col) {
		if (this.data[row - 1][col - 1] != '-') {
			return false;
		}
		this.data[row - 1][col - 1] = currentPlayer.getName();
		lastcol = col - 1;
		lastrow = row - 1;
		turn ++;
		return true;
	}

	public boolean checkRow() {
		for (int col = 0; col < this.data[lastrow].length; col++) {
			if (this.data[lastrow][col] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;

	}

	public boolean checkCol() {
		for (int row = 0; row < this.data.length; row++) {
			if (this.data[row][lastcol] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;

	}

	public boolean checkX1() {
		for (int i = 0; i < this.data.length; i++) {
			if (this.data[i][i] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}

	public boolean checkX2() {
		for (int i = 0; i < this.data.length; i++) {
			if (this.data[i][this.data.length - i - 1] != currentPlayer.getName()) {
				return false;
			}
		}
		return true;
	}
	public void updateStat() {
		if(this.one == this.win) {
			this.one.win();
			this.two.loser();
		}else if (this.two == this.win) {
			this.two.win();
			this.one.loser();
		}else {
			this.two.draw();
			this.one.draw();
		}
	}
	
	public boolean checkWin() {
		if (checkCol() || checkRow() || checkX1() || checkX2()) {
			this.win = currentPlayer;
			return true;
		}
		if(turn==9) {
			return true;
		}
		return false;
	}
	public Player getWinner() {
		return win;
		
	}
}


