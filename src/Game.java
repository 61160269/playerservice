/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows10
 */
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Windows10
 */
public class Game {
     Scanner kb = new Scanner(System.in);
    Table table = null;
    private int row;
    private int col;
    Player o = null;
    Player x = null;

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');
    }

    public void run() {
        while (true) {
            this.run1();
            if (!Continue()) {
                return;
            }

        }
    }

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    public void newGame() {
        if (getRandomNumber(1, 100) % 2 == 0) {
            this.table = new Table(o, x);
        } else {
            this.table = new Table(x, o);
        }

    }

    public void run1() {

        this.showWelcome();
        this.newGame();
        while (true) {
            this.showTable();
            this.showTurn();
            inputRowCol();
            if (table.checkWin()) {
                if (table.getWinner() != null) {
                    System.out.println(table.getWinner().getName() + "  ****Win****");
                } else {
                    System.out.println("  ****Draw****");
                    this.showStat();
                }
                this.showStat();
                return;
            }
            table.switchPlayer();
        }

    }

    public void showWelcome() {
        System.out.println("Welcome to OX game");
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " Turn");

    }

    private void showTable() {
        char[][] data = this.table.getData();
        System.out.println("-------------");
        for (int row = 0; row < data.length; row++) {
            for (col = 0; col < data.length; col++) {
                System.out.print("|" + " " + data[row][col] + " ");
            }
            System.out.println("|\n-------------");
        }
    }

    private void Input() {
        while (true) {
            try {
                System.out.print("Please input row col : ");
                row = kb.nextInt();
                col = kb.nextInt();
                if (kb.hasNext()) {
                    System.out.println(" Please input Number (1-3) !!! : ");
                    continue;
                }
                return;

            } catch (InputMismatchException c) {
                kb.next();
                System.out.println("Please input Number!!!! : ");
            }
        }
    }

    private void inputRowCol() {
        while (true) {
            this.Input();
            try {
                if (table.setRowCol(row, col)) {
                    return;
                }

            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Please input Number (1-3) !!! : ");
            }

        }

    }

    private boolean Continue() {
        while (true) {
            System.out.print("Continue Y/n : ");
            String a = kb.next();
            if (a.equals("Y")) {
                return false;
            } else if (a.equals("n")) {
                System.out.println("***** GAME OVER *****");
                return true;
            }
        }

    }

    private void showStat() {
        System.out.println(o.getName() + "(win,loser,draw):" + o.getWin() + ", " + o.getLoser() + ", " + o.getDraw());
        System.out.println(x.getName() + "(win,loser,draw):" + x.getWin() + ", " + x.getLoser() + ", " + x.getDraw());
    }

}

